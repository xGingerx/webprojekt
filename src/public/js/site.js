function validateInput(input) {
    const pattern = /^\d+(\.\d{1,2})?$/;
    const value = input.value;

    if (!pattern.test(value)) {
        input.setCustomValidity("Please enter a valid decimal number and positive number.");
    } else {
        input.setCustomValidity("");
    }
}

function validateForm() {
    const inputs = document.querySelectorAll('input[name="victory"]');
    for (const input of inputs) {
        validateInput(input);
    }

    
    return Array.from(inputs).every((input) => input.checkValidity());
}