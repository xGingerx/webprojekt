export function kirkmanPairing(playerIds: number[], rd: number): number[][] {
    var n = playerIds.length;
    if(n % 2 != 0) {
      n = n + 1
    }
    
    const idex: [number, number] = [rd, rd];
    const pair: number[][] = [];
  
    for (let i = 0; i < n / 2; i++) {
      if (idex[0] === idex[1]) {
        if(!(playerIds[idex[0] - 1] == undefined || playerIds[n - 1] == undefined)) {
          pair.push([playerIds[idex[0] - 1], playerIds[n - 1]]);
        }
        
      } else {
        if(!(playerIds[idex[0] - 1] == undefined || playerIds[idex[1] - 1] == undefined))
        pair.push([playerIds[idex[0] - 1], playerIds[idex[1] - 1]]);     
      }
  
      idex[0]--;
      idex[1]++;
  
      if (idex[0] < 1) {
        idex[0] = n - 1;
      }
  
      if (idex[1] > n) {
        idex[1] = 1;
      }
    }
    return pair;
  }

  

