import express from 'express';
import fs from 'fs';
import path from 'path'
import https from 'https';
import { auth, requiresAuth } from 'express-openid-connect'; 
import dotenv from 'dotenv'
import {client, createTables} from './database';
import { kirkmanPairing } from './timetable';

dotenv.config()


const app = express();
app.set("views", path.join(__dirname, "views"));
app.set('view engine', 'pug');


createTables();
const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;

const config = {
    authRequired : false,
    idpLogout : true, //login not only from the app, but also from identity provider
    secret: process.env.SECRET,
    baseURL: externalUrl || `https://localhost:${port}`,
    clientID: process.env.CLIENT_ID,
    issuerBaseURL: process.env.BASE_URL,
    clientSecret: process.env.CLIENT_SECRET,
    authorizationParams: {
    response_type: 'code'
    ,
    //scope: "openid profile email"
    },
};
app.use(auth(config));
app.use(express.urlencoded({ extended: true }));

app.get('/',  function (req, res) {
    let username : string | undefined;
    if (req.oidc.isAuthenticated()) {
      username = req.oidc.user?.name ?? req.oidc.user?.sub;
    
    }
    res.render('home', {username});
  });
  
  app.get('/main', requiresAuth(), async function (req, res) {       
      const user = JSON.stringify(req.oidc.user); 
      const userJSON = JSON.parse(user)  
      const result = await client.query(`SELECT competition.name, competition.url FROM competition WHERE email = '${userJSON.email}'`)
      var rows = result.rows
      res.render('main', {user, userJSON, rows}); 
  });

  app.get('/create', requiresAuth(), async function(req, res) {
    const user = JSON.stringify(req.oidc.user); 
    const userJSON = JSON.parse(user) 
    const err = null    
    const created = req.query.created === 'true';
    var koloRows;
    var url;
    if(created) {
        const id = req.query.competitionId;
        const result = await client.query(`SELECT kolo.kolo_num as kolo_num,
                                                  competition.name as comp_name,
                                                  competition.url,
                                                  person1.name as pers1_name,
                                                  person2.name as pers2_name,
                                                  competition.victory_points,
                                                  competition.defeat_points,
                                                  competition.remi_points	
                                              FROM kolo 
                                              JOIN competition ON competition.id = kolo.id_competition
                                              JOIN person AS person1 ON id_player1 = person1.id
                                              JOIN person AS person2 ON id_player2 = person2.id
                                              WHERE kolo.id_competition = ${id} ORDER BY kolo_num`)
        koloRows = result.rows;
    }
    
    res.render('create', {user, userJSON, err, created, koloRows}); 
  });


// stvaranje natjecanja
  app.post('/submit-form', requiresAuth(), async (req, res)=>{
    var email;
    if(req.oidc.user){
      email = req.oidc.user.email;
    }
    var playersArr = req.body.players.split(/[;\n]/).map(function(player: string) {
      return player.trim();
    });
    var points = req.body.points.split('/').map((point:string)=>{
      return Number(point.trim())
    })

    // add competition in database
    const text_comp = 'SELECT MAX(id) as max_id FROM competition';
    
    var result = await client.query(text_comp);
    var next_id_comp = result.rows[0].max_id + 1;
    var comp_name = req.body.compName.trim();
    var url = req.protocol + '://' + req.get('host') + '/comp/' + comp_name + '/' + next_id_comp
    try{
      await client.query(`INSERT INTO "competition" 
          VALUES ($1, $2, $3, $4, $5, $6, $7)`,
          [next_id_comp, comp_name, email, url, points[0], points[2], points[1]])
    } catch (err) {
        console.log(err)
    }

    // add players in database
    const text_pers = 'SELECT MAX(id) as max_id FROM person';
    result = await client.query(text_pers);
    var next_id = result.rows[0].max_id;
    var playersArr_id: number[] = []
    playersArr = playersArr.filter((player: string) => player.trim().length != 0)
    for (let i = 0; i < playersArr.length; i++) {
      next_id += 1;
      try{
        await client.query(`INSERT INTO "person" 
            VALUES ($1, $2, $3, $4)`,
            [next_id, playersArr[i], next_id_comp, 0])
        playersArr_id.push(next_id)
      } catch (err) {
          console.log(err)
      }
      
    }

    // add kolo in database and create schedule
    var numOfKola = playersArr.length % 2 == 0 ? playersArr.length - 1 : playersArr.length;
    
    var koloArray = []
    for (let i = 1; i <= numOfKola; i++) {
        koloArray = kirkmanPairing(playersArr_id, i);
        const text_kolo = 'SELECT MAX(id) as max_id FROM kolo';
        var result = await client.query(text_kolo);
        var next_id = result.rows[0].max_id;
        koloArray.forEach(async (element) => {
            next_id++;
            try{
              await client.query(`INSERT INTO "kolo" 
                  VALUES ($1, $2, $3, $4, $5, $6, $7)`,
                  [next_id, i, next_id_comp, element[0], element[1], null, false])
            } catch (err) {
                console.log(err)
            }
        });
          
        }

    res.redirect(`/create?created=true&competitionId=${next_id_comp}`);
  })


  app.get("/comp/:name/:id", async (req, res) => {
    let user = req.oidc.user;
    const id = req.params.id;
    const result = await client.query(`SELECT kolo.kolo_num as kolo_num,
                                              kolo.winner as winner,
                                              kolo.remi as remi,
                                              kolo.id as kolo_id,
                                                  competition.name as comp_name,
                                                  competition.url,
                                                  kolo.id_player1,
                                                  kolo.id_player2,
                                                  person1.name as pers1_name,
                                                  person2.name as pers2_name,
                                                  competition.victory_points,
                                                  competition.defeat_points,
                                                  competition.remi_points	
                                              FROM kolo 
                                              JOIN competition ON competition.id = kolo.id_competition
                                              JOIN person AS person1 ON id_player1 = person1.id
                                              JOIN person AS person2 ON id_player2 = person2.id
                                              WHERE kolo.id_competition = ${id} ORDER BY kolo_num`)
    var koloRows = result.rows;
    
    const resultPerson = await client.query (`SELECT * FROM person WHERE id_competition = ${id} ORDER BY points DESC, name`)
    var personPoints = resultPerson.rows
    var creatorEmail = await client.query (`SELECT email FROM competition WHERE id = ${id}`)
    creatorEmail = creatorEmail.rows[0].email;
    if(user && creatorEmail != user.email){
      user = undefined
    }
    res.render('comp', {user, koloRows, personPoints, id})
  })

  app.post("/submit-update/:id", requiresAuth(), async (req, res) => {
    const id = req.params.id;
    try{
      await client.query(`UPDATE "competition"
          SET victory_points = $1, 
              defeat_points = $2, 
              remi_points = $3
          WHERE id = ${id}`,
          [req.body.victory, req.body.defeat, req.body.remi])
    } catch (err) {
        console.log(err)
    }

    var body = req.body;
    for (const key in body) {
      var winner;
      var remi;
      var id_kolo;
      if (key.startsWith("winner_")) {
        id_kolo = Number(key.split('_')[1]);
        var value = body[key]
        if(value == 'undefined'){
          winner = null;
          remi = false;
        } else if( value == 'remi') {
          remi = true;
          winner = null
        } else{
          remi = false
          winner = Number(value)
        }
        try{
          await client.query(`UPDATE "kolo"
              SET winner = $1, 
                  remi = $2
              WHERE id = ${id_kolo}`,
              [winner, remi])
        } catch (err) {
            console.log(err)
        }
      }
    }

    const result = await client.query(`SELECT id_player1, id_player2, winner, remi, victory_points, defeat_points, remi_points
                                    FROM kolo join competition on kolo.id_competition = competition.id
                                    WHERE id_competition = ${id}`)
    var result2 = result.rows;

    var scores: { [key:string]:number} = {}
    const victoryPoints = parseFloat(result2[0].victory_points);
    const defeatPoints = parseFloat(result2[0].defeat_points);
    const remiPoints = parseFloat(result2[0].remi_points);
    for (let row of result2){
      if(!(row.id_player1 in scores)){
        scores[row.id_player1] = 0
      }
      if(!(row.id_player2 in scores)){
        scores[row.id_player2] = 0
      }
      if(row.remi == false && row.winner == null) {
        continue;
      }
      if(row.remi == true) {
        scores[row.id_player1] += remiPoints;
        scores[row.id_player2] += remiPoints;
      }
      if(row.remi == false && row.winner != null) {
        if(row.winner == row.id_player1) {
          scores[row.id_player1] += victoryPoints;
          scores[row.id_player2] += defeatPoints;
        } else if(row.winner == row.id_player2) {
          scores[row.id_player1] += defeatPoints;
          scores[row.id_player2] += victoryPoints;
        }
      }
    }
    for(let key in scores){
      if(scores[key] < 0) scores[key] = 0;
      let id = parseInt(key)
      try{
        await client.query(`UPDATE "person"
            SET points = $1 
            WHERE id = ${id}`,
            [scores[key]])
      } catch (err) {
          console.log(err)
      }
    }
    res.redirect(`/comp/:name/${id}`)
  })

  
  app.get("/sign-up", (req, res) => {
    res.oidc.login({
      returnTo: '/main',
      authorizationParams: {      
        screen_hint: "signup",
      },
    });
  });
  
  if (externalUrl) {
    const hostname = '0.0.0.0'; //ne 127.0.0.1
    app.listen(port, hostname, () => {
      console.log(`Server locally running at http://${hostname}:${port}/ and from outside on ${externalUrl}`);
    });
  } else {
      https.createServer({
        key: fs.readFileSync('server.key'),
        cert: fs.readFileSync('server.cert')
    }, app)
    .listen(port, function () {
    console.log(`Server running at https://localhost:${port}/`);
  });
}

