const {Client} = require('pg')

export const client = new Client({
    host:process.env.DB_HOST,
    user:process.env.DB_USER,
    port:process.env.DB_PORT,
    password:process.env.DB_PASS,
    database:process.env.DB_DATABASE
})


const createPerson = 
`CREATE TABLE IF NOT EXISTS "person" (
    "id" INT, 
    "name" VARCHAR(100) NOT NULL,
    "id_competition" INT,
    "points" DECIMAL(10,2),
    PRIMARY KEY("id"),
    FOREIGN KEY("id_competition") REFERENCES competition ("id")
);`

const createCompetition = 
`CREATE TABLE IF NOT EXISTS "competition" (
    "id" INT,
    "name" VARCHAR(100) NOT NULL,
    "email" TEXT NOT NULL,
    "url" TEXT NOT NULL,
    "victory_points" DECIMAL NOT NULL,
    "defeat_points" DECIMAL NOT NULL,
    "remi_points" DECIMAL NOT NULL,
    PRIMARY KEY("id")
);`

const createKolo = 
`CREATE TABLE IF NOT EXISTS "kolo" (
    "id" INT,
    "kolo_num" INT,
    "id_competition" INT NOT NULL,
    "id_player1" INT NOT NULL,
    "id_player2" INT NOT NULL,
    "winner" INT,
    "remi" BOOLEAN,
    PRIMARY KEY("id"),
    FOREIGN KEY("id_player1") REFERENCES "person" ("id"),
    FOREIGN KEY("id_player2") REFERENCES "person" ("id"),
    FOREIGN KEY("id_competition") REFERENCES "competition"("id")
);`



export const createTables = async () => {
    await client.connect();
    try {
        await client.query(createCompetition)
        await client.query(createPerson)
        await client.query(createKolo)
    } catch(err) {
        console.log(err)
    }   
    
}



